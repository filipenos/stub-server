## Overview ##
You already had to create a server that should communicate with a legacy server, and you did not have access to that server. 
You've ever had to run a test that should communicate with the web but you were offline. 
Well these use cases are normal for any web developer, so this made ​​this solution. 
With a server stubs you can start it to run unit and integration tests. A simple solution that solves a variety of problems

##How it work##
You define a configuration file which should contain a list of returns for each accessed path, with a return code, an access method and a body, the body can optionally be a json, each path will have accessed a valid return to the server

###Sample of config file###
```
#!json
[{
  "method": "POST",
  "path": "/api/create",
  "body" "create successfully"
}, {
  "method": "GET",
  "path": "/api/get",
  "body": "path_to_json_file",
  "type": "json"
}]
```

## How to get ##
```
#!shell
go get bitbucket.org/filipenos/stub-server
```

## How to run ##
```
#!shell
stub-server -port=8090 -conf=config.json
```